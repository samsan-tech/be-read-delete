var cron = require("node-cron");
var async = require("async");
const redisDb = require("../redis");
const MongoClient = require("mongodb").MongoClient;
const db = require("../db");

exports.runJob = () => {
  cron.schedule(
    "0 1 * * *",
    () => {
      let dateNow = parseInt(new Date().getTime() / 1000).toFixed(0);

      var redis = redisDb();

      redis.on("error", function (err) {
        console.log("Error " + err);
      });

      redis.keys("*", function (err, keys) {
        if (err) return console.log(err);
        if (keys) {
          async.map(
            keys,
            function (key, cb) {
              redis.get(key, function (error, value) {
                if (error) return cb(error);
                let updatedUser = [];
                if (value <= dateNow) {
                  updatedUser.push({ name: key, date: value });
                }

                cb(null, updatedUser);
              });
            },
            function (error, results) {
              var client = db();
              if (error) return console.log(error);
              // console.log(results);
              async.map(results, function (result) {
                client.connect((err) => {
                  if (result.length !== 0) {
                    const collection = client.db("tkai").collection("Tkai");
                    let filter = { name: result[0].name };
                    const update = { $set: { need_update: true } };

                    collection.findOneAndUpdate(
                      filter,
                      update,
                      function (err, result) {
                        if (err) {
                          console.log("ini error");
                          console.log(err);
                          console.log(filter);
                          console.log("yang error si orang di atas saya");
                        } else {
                          console.log(result);
                          console.log("berhasil di update");
                        }
                      }
                    );
                  }
                });
                client.close();
              });

              //TODO
              //UPDATE SHOULDUPDATE IN MONGODB
            }
          );
        }
      });

      console.log("Runing a job at 01:00 at Asia/Jakarta timezone");
    },
    {
      scheduled: true,
      timezone: "Asia/Jakarta",
    }
  );
};
