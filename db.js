const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");
const db_user = process.env.DB_USERNAME;
const db_pass = process.env.DB_PASSWORD;
const redis_auth = process.env.REDIS_AUTH;

module.exports = () => {
  // Connection URL
  const uri = `mongodb://${db_user}:${db_pass}@34.70.144.224:27016`;
  const mongooseOpts = {
    useNewUrlParser: true,
    autoReconnect: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000,
    poolSize: 10,
  };

  // Database Name

  // Create a new MongoClient
  var client = new MongoClient(uri, mongooseOpts);
  return client;
};

// Use connect method to connect to the Server
