const express = require("express");
const bodyParser = require("body-parser");
var async = require("async");
const { runJob } = require("./utils/job");
var cors = require("cors");
const MongoClient = require("mongodb").MongoClient;
const db = require("./db");
const redisDb = require("./redis");
// Constants
const PORT = 8000;

// App
const app = express();
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.json());

//Running cron job
runJob();

app.get("/api", (req, res) => {
  var client = db();
  client.connect((err) => {
    const collection = client.db("tkai").collection("Tkai");
    // perform actions on the collection object
    var myObj = { name: "tes", age: 29, will_update: false };
    collection.insertOne(myObj, (err, res) => {
      if (err) throw err;
      console.log("Inserted");
    });
    client.close();
  });
  console.log(client);

  res.status(200).json({
    message: "Hello World!",
  });
});

app.get("/api/users", (req, res) => {
  var client = db();
  client.connect((err) => {
    const collection = client.db("tkai").collection("Tkai");
    // perform actions on the collection object
    collection.find({}).toArray((err, results) => {
      if (err) throw err;
      res.status(200).json({
        data: results,
      });
    });
    client.close();
  });
});

app.get("/api/users/:id", (req, res) => {
  let id = new ObjectID(req.params.id);
  var client = db();
  client.connect((err) => {
    const collection = client.db("tkai").collection("Tkai");
    // perform actions on the collection object
    collection.findOne({ _id: id }, function (err, results) {
      if (err) throw err;
      res.status(200).json({
        data: results,
      });
    });
    client.close();
  });
});

var ObjectID = require("mongodb").ObjectID;
app.delete("/api/users", (req, res) => {
  var client = db();
  var redis = redisDb();
  redis.on("error", function (err) {
    console.log("Error " + err);
  });
  let name = req.body["name"];
  let id = new ObjectID(req.body["id"]);
  client.connect((err) => {
    const collection = client.db("tkai").collection("Tkai");
    collection.deleteOne({ _id: id }, function (err, obj) {
      if (err) {
        res.json({
          message: "gagal",
        });
      } else {
        redis.del(name, function (err, response) {
          if (response == 1) {
            console.log("Deleted Successfully!");
            res.status(200).json({
              message: "Data user dengan id " + id + " telah dihapus",
            });
          } else {
            console.log("Cannot delete");
            res.json({
              message: "gagal",
            });
          }
        });
      }
    });
    client.close();
  });
});

app.post("/api/seeder", (req, res) => {
  var redis = redisDb();
  redis.on("error", function (err) {
    console.log("Error " + err);
  });
  let dateNow = parseInt(new Date().getTime() / 1000).toFixed(0);

  // let dateNow = Math.floor(new Date().getTime() / 1000);

  const datas = [
    { name: "udin", date: dateNow },
    { name: "udiasdn", date: dateNow },
  ];
  async.map(
    datas,
    function (data, cb) {
      redis.set(data.name, data.date, redis.print);
      cb(null, datas);
    },
    function (error, results) {
      if (error) return console.log(error);
      res.status(200).json({
        message: "seed completed",
        data: results[0],
      });
    }
  );
});

app.get("/api/seeder", (req, res) => {
  var redis = redisDb();
  redis.on("error", function (err) {
    console.log("Error " + err);
  });

  redis.keys("*", function (err, keys) {
    if (err) {
      res.json({
        message: err,
      });
    }

    if (keys) {
      async.map(
        keys,
        function (key, cb) {
          redis.get(key, function (error, value) {
            if (error) return cb(error);
            console.log(key, value);
            var users = {};
            users["name"] = key;
            users["date"] = value;
            cb(null, users);
          });
        },
        function (error, results) {
          if (error) return console.log(error);
          res.status(200).json({
            data: results,
          });
        }
      );
    }
  });
});

app.delete("/api/seeder", (req, res) => {
  var redis = redisDb();
  redis.on("error", function (err) {
    console.log("Error " + err);
  });
  redis.flushdb(function (err, succeeded) {
    if (err) {
      console.log(err);
    }
    res.status(200).json({
      message: "deleted",
    });
    // will be true if successfull
  });
});

app.listen(PORT);
console.log("Server listening on port: " + PORT);
