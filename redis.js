var redis = require("redis");
const redis_auth = process.env.REDIS_AUTH;

module.exports = () => {
  // Connection URL
  var client = redis.createClient({
    host: "34.70.144.224",
    port: 6379,
    password: `${redis_auth}`,
  });

  // Database Name

  return client;
};

// Use connect method to connect to the Server
